import React from 'react';

const footer = (props) => {
  return (
    <footer className="page-footer teal darken-2">
      <div className="footer-copyright">
        <div className="container">© {new Date().getFullYear()} {props.title}</div>
      </div>
    </footer>
  )
}

export default footer;