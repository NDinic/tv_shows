import Axios from 'axios';
const BASE_URL = 'http://api.tvmaze.com/shows';

export const getShowsList = () => {
  let updatedData = [];
  return (
    Axios(BASE_URL)
      .then(res => {
        updatedData = res.data.map(data => {
          return {
            ...data
          }
        })
        updatedData = updatedData.splice(0, 64);
        return updatedData;
      })
      .catch(err => {
        console.log(err)
      })
  )
}
export const getCardDetail = (id) => {
// console.log(BASE_URL + '/' + id + '?embed[]=seasons&embed[]=cast')
  return (
    Axios(BASE_URL + '/' + id + '?embed[]=seasons&embed[]=cast')
    .then(res => res)
    .catch(err=>{
      console.log(err)
    })
  ) 
}