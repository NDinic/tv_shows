import React from 'react';

const searchBar = (props) => {
  return (
    <form className="col s12">
      <div className="input-field col s12">
        <i className="material-icons prefix">search</i>
        <input
          onChange={props.changed}
          value={props.textInput}
          id="search"
          type="search"
          className="validate"
          placeholder="Search shows"
        />
      </div>
    </form>
  )
}

export default searchBar;