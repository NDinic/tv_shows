import React from 'react';

const GenresList = (props) => {
  return props.genres.map((genre, i) => (
    <div className="card chip" key={i}>
      {genre}
    </div>
  ))
}
export default GenresList;