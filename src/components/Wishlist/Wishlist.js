import React, { Fragment } from 'react';
import './WishList.css';

const wishList = (props) => {

  const userWishList = localStorage.getItem('wish');
  let wishData = null;
  if (userWishList) {
    userWishList.map(wish => {
      wishData = wish
      return wishData
    })
  }
  console.log(props.wishlist)
  wishData = props.wishlist.map(wish => {
    // return wishD.wishlist.map( wish => {
      return (
        <div className="col s12 m4 l3" key={wish.id + Math.random() }>
        
          <div className="card hoverable">
            <div className="card-image">
              <img src={wish.image.medium} alt={wish.name} title={wish.name} />
              <span className="btn-floating halfway-fab">{wish.rating.average}</span>
            </div>
            <div className="card-content">
              <p className="card-title grey-text text-darken-4 truncate">{wish.name}</p>
            </div>
          </div>
        </div>
      )
    // })

    
  })
  return (
    <Fragment>
      <div>
        <h2>My wishlist</h2>
        <button className="btn right" type="button" onClick={props.saveWishlist}>Save Wishlist</button>
        <hr />
      </div>
      <div className="row">{wishData}</div>

    </Fragment>
  )
}

export default wishList;