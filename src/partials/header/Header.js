import React, { Component, Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import Login from '../../components/Login/Login';


class Header extends Component {
    render() {
        console.log(this.props)
        let log = <NavLink to="/" onClick={this.props.loginHandler}>Login</NavLink>
        if (!this.props.loginShow && this.props.isLoggedIn) {
            log = <NavLink to="/" onClick={this.props.logOutHandler}>Logout</NavLink>
        }
        let wish = null;
        if (!this.props.loginShow && this.props.isLoggedIn) {
            wish = <NavLink to="/wishlist">Wishlist <span>{this.props.wishLength.length}</span></NavLink>
        }
        return (
            <Fragment>
                <header>
                    <nav className="teal darken-2">
                        <div className="container">
                            <div className="nav-wrapper">
                                <NavLink to="/" className="brand-logo card-title">
                                    {this.props.title}
                                </NavLink>
                                <ul className="right">
                                    <li>
                                        {wish}
                                    </li>
                                    <li>
                                        <NavLink to="/about">About</NavLink>
                                    </li>
                                    <li>
                                        {log}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </header>
                <Login
                    isLoggedIn={this.props.isLoggedIn}
                    loginShow={this.props.loginShow}
                    loginHandler={this.props.loginHandler}
                    hideLogin={this.props.hideLogin}
                    logOutHandler={this.props.logOutHandler}
                    getLoggedUserData={this.props.getLoggedUserData}
                />
            </Fragment>
        )
    }
}

export default Header;

