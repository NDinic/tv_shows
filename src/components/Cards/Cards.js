import React, { Component, Fragment } from 'react';
import { getShowsList } from '../../services/Services';
import Loader from '../../partials/loader/Loader';
import SearchBar from './SearchBar';

import { Link } from 'react-router-dom';


class Cards extends Component {
  state = {
    data: null,
    loader: true,
    filteredData: null,
    textInput: null,
    limit: 8,
    limitInit: 8
  }
  componentDidMount() {
    getShowsList()
      .then(show => {
        this.setState({
          data: show,
          loader: false,
          filteredData: show
        });
      })
      .catch(err => {
        console.log(err)
      })
  }

  onInputHandler = e => {
    let inputDataVal = e.target.value;

    let filterData = this.state.data.filter(city => {
      return city.name.toLowerCase().startsWith(inputDataVal.toLowerCase())
    })
    this.setState({
      filteredData: filterData,
      textInput: inputDataVal,
      limit: this.state.limitInit
    })
  }

  loadMore = () => {
    this.setState(prevState => {
      return { limit: prevState.limit + this.state.limitInit }
    })
  }

  render() {
    
    let listCards = <Loader isLoading={this.state.loader} />
    if (!this.state.loader) {

      listCards = this.state.filteredData.slice(0, this.state.limit).map(card => {
        return (
          <Fragment key={card.id}>
            <div className="col s12 m4 l3">
              <div className="card hoverable">
                <Link to={`/card-detail/${card.id}`} key={card.id}>
                  <div className="card-image">
                    <img src={card.image.medium} alt={card.name} title={card.name} />
                    <span className="btn-floating halfway-fab">{card.rating.average}</span>
                  </div>
                  <div className="card-content">
                    <p className="card-title grey-text text-darken-4 truncate">{card.name}</p>
                  </div>
                </Link>
                {this.props.isUserLogged ?
                  <span className="btn-floating halfway-fab add-wishlist" onClick={() => this.props.AddToWish(card, card.id)}><i className="material-icons">add</i></span>
                  : null
                }
              </div>
            </div>
          </Fragment>
        )
      })
    }
    return (
      <div className="row">
        {!this.state.loader ? <SearchBar
          changed={this.onInputHandler}
          value={this.state.textInput}
        /> : null}

        {listCards}
        {this.state.filteredData && this.state.limit < this.state.filteredData.length ?
          <div className="loadMoreWrapper"><button type="button" className="loadMore hoverable" onClick={this.loadMore}>Load more</button></div> : null}
      </div>
    )
  }
}
export default Cards;