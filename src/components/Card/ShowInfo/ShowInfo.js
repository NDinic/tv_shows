import React from 'react';
import GenresList from '../GenresList/GenresList';
import ShowActors from '../ShowActors/ShowActors';
import { Link } from 'react-router-dom';

const showInfo = (props) => {
  return (
    <div className="row">
      <div className="col s5">
        <div className="card">
          <div className="card-image main-image">
            <img src={props.show.image.medium} alt="" />
          </div>
        </div>
      </div>
      <div className="col s6 offset-s1">
        <h3>{props.show.name.toUpperCase()}</h3>
        <br />
        <GenresList genres={props.show.genres} />
        <div className="flow-text">{props.show.summary.replace(/(<([^>]+)>)/gi, '')}</div>
      </div>
      <div className="col s12">
        <hr />
        <div className="row">
          <h4 className="left">Actors</h4>
          <Link to="" className="right" onClick={props.view}>
            {props.icon()}
          </Link>
        </div>
        <div className="row">
          <ShowActors
            actors={props.show._embedded.cast}
            isGrid={props.isGrid} />
        </div>
      </div>
    </div>
  )
}

export default showInfo;