import React from 'react';
// import './Button.css';

const button = (props) => {
  return (
    <button
      type={props.buttonType}
      className={props.buttonClass}
      onClick={props.clickButton}
      disabled={props.disabled}>
      {props.children}
    </button>
  )
}

export default button;