import React from 'react';
import './Input.css';

const input = (props) => {
  // console.log(props)
  // let inputElement = null;
  // let inputClasses = [InputElement];
  let inputClasses = []
  if(props.invalid && props.shouldValidate && props.touched) {
    inputClasses.push('error')
  }
  
  return (
    <input
      className={inputClasses.join(' ')}
      {...props.elementConfig}
      value={props.value}
      onChange={props.changed} />
  )
}

export default input;