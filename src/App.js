import React, { Component, Fragment } from 'react';
import './App.css';
import Header from './partials/header/Header';
import Footer from './partials/footer/Footer';
import Cards from './components/Cards/Cards';
import AboutUs from './components/AboutUs/AboutUs';
import CardDetail from './components/Card/CardDetail';
import Wishlist from './components/Wishlist/Wishlist';
import { Route, Switch } from 'react-router-dom';
import axios from 'axios';

class App extends Component {
  state = {
    showLogin: false,
    isLoggedIn: false,
    wishlist: []
  }
  componentDidMount() {
    this.getLoggedUserDataHandler()
    // const userWishList = localStorage.getItem('wish');

    // const queryParams = '?auth=' + token + '&orderBy="userId"&equalTo="' + userId + '"'; 
    // axios.get('/orders.json' + queryParams)
    // if (userWishList) {
    //   this.setState({
    //     wishlist: userWishList
    //   })
    // }
  }

  getLoggedUserDataHandler = () => {
    
    const token = localStorage.getItem('token');
    const userId = localStorage.getItem('userId');
    console.log(token)
    console.log(userId)
    if (token && userId) {

      // const queryParams = '?auth=' + token + '&orderBy="userId"&equalTo="' + userId + '"'; 
      // axios.get('/orders.json' + queryParams)

    const queryParams = '?auth=' + token + '&orderBy="userId"&equalTo="' + userId + '"';
    axios.get('https://tv-series-16a32.firebaseio.com/wishlist.json' + queryParams)
    .then(res => {
      console.log(res.data)
      const fetchedWishes = [];
      for (let key in res.data) {
        console.log(key)
          for (let wish in res.data[key].wishlist)
        // console.log(res.data[key].wishlist[wish])
        fetchedWishes.push({
          ...res.data[key].wishlist[wish],
          id: key 
        })
      }
      this.setState({
        showLogin: false,
        isLoggedIn: true,
        wishlist: fetchedWishes
      })
    })
      .catch(err => console.log(err))
    }
  }


  showLoginHandler = (event) => {
    event.preventDefault()
    this.setState(prevState => {
      return { showLogin: !prevState.showLogin }
    })
  }

  logOutHandler = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('wish');

    this.setState({
      showLogin: false,
      isLoggedIn: false

    })
  }

  hideLoginHandler = (loggedIn) => {
    if (loggedIn) {
      this.setState({
        showLogin: false,
        isLoggedIn: true
      })
    }
  }

  getYourPriceHandler = (price, id) => {
    let yourPrice = [...this.state.yourPrice];

    let calcYourPrice = {
      id: id,
      price:
        Number.parseFloat(price).toFixed(2) *
        Number.parseFloat(this.state.amount).toFixed(2)
    };

    yourPrice.push(calcYourPrice);
    this.setState({
      yourPrice: yourPrice,
      initPrice: 0
    });
  };



  AddToWishlist = (wish, id) => {
    console.log(wish)
    console.log(id)
    let wishListArr = [...this.state.wishlist];

    wishListArr.push(wish)
    this.setState({ wishlist: wishListArr }, () => console.log(this.state.wishlist))
    // const userWish = localStorage.setItem('wish', this.state.wishlist);
    // if (userWish) {
    //   this.setState({
    //     wishlist: userWish
    //   })
    // }

  }
  saveWishHandler = () => {
    const token = localStorage.getItem('token');
    const userId = localStorage.getItem('userId');
    console.log(userId)
    axios.post('https://tv-series-16a32.firebaseio.com/wishlist.json?auth=' + token, {
      userId: userId,
      wishlist: this.state.wishlist
    })
      .then(response => response)
      .catch(err => err)
  }
  render() {

    return (
      <Fragment>
        <Header
          title="TV Series"
          isLoggedIn={this.state.isLoggedIn}
          loginShow={this.state.showLogin}
          loginHandler={this.showLoginHandler}
          hideLogin={this.hideLoginHandler}
          logOutHandler={this.logOutHandler}
          wishLength={this.state.wishlist}
          getLoggedUserData={this.getLoggedUserDataHandler}
        />
        <div className="container">
          <Switch>
            <Route
              exact
              path='/'
              render={(props) => <Cards {...props} isLoginVisible={this.state.showLogin} isUserLogged={this.state.isLoggedIn} AddToWish={this.AddToWishlist} />}
            />
            <Route path="/card-detail/:id" component={CardDetail} />
            <Route path="/about" component={AboutUs} />
            <Route
              path='/wishlist'
              render={(props) => <Wishlist {...props} wishlist={this.state.wishlist} saveWishlist={this.saveWishHandler} />}
            />
          </Switch>
        </div>
        <Footer title="TV shows - Movies and series" />
      </Fragment>
    )
  }
}

export default App;