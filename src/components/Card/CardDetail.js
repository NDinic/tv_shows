import React, { Component, Fragment } from 'react';
import { getCardDetail } from '../../services/Services';
import Loader from '../../partials/loader/Loader';
import ShowInfo from './ShowInfo/ShowInfo';
import './Card.css';
class CardDetails extends Component {
  state = {
    show: null,
    loader: true,
    grid: true
  }

  componentDidMount() {
    getCardDetail(this.props.match.params.id)
      .then(show => {
        this.setState({
          show: show.data,
          loader: false
        })
      })
      .catch(err => {
        console.log(err)
      })
  }
  changeView = (event) => {
    event.preventDefault();
    let isGrid = this.state.grid;
    this.setState({ grid: !isGrid });

  }
  renderIcon = () => {
    return !this.state.grid ? (
      <i className="medium material-icons">view_list</i>
    ) : (
        <i className="medium material-icons">view_module</i>
      )
  }

  render() {
    let showInfo = <Loader isLoading={this.state.loader} />
    if (this.state.show) {
      showInfo = <ShowInfo
        show={this.state.show}
        view={this.changeView}
        icon={this.renderIcon}
        isGrid={this.state.grid}
      />
    }
    return (
      <Fragment>
        {showInfo}
      </Fragment>
    )
  }
}

export default CardDetails;