import React from 'react';

const showList = (props) => {
  var actorList = props.actors.map(actor => (
    <div className="col s12 m2" key={actor.character.id}>
      <div className="card ">
        <div className="card-image">
          <img className="z-depth-2" src={actor.person.image.medium} alt="" />
          <span className="card-title">{actor.person.name}</span>
        </div>
      </div>
    </div>
  ))
  if (!props.isGrid) {
    actorList = (
      <ul className="collection">
        {props.actors.map(actor => (
          <li className="collection-item avatar valign-wrapper" key={actor.character.id}>
            <img src={actor.person.image.original} alt="" className="circle responsive-img" />
            <h4>{actor.person.name}</h4>
          </li>
        ))}
      </ul>
    )
  }
  return (
    <div>{actorList}</div>
  )
}

export default showList;