import React, { Component, Fragment } from 'react';
import Backdrop from '../UI/Backdrop/Backdrop';
import Button from '../UI/Button/Button';
import Input from '../UI/Input/input';
import axios from 'axios';

import './Login.css';

class Login extends Component {
  state = {
    users: {
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'email',
          placeholder: 'Email'
        },
        value: '',
        validation: {
          required: true,
          isEmail: true
        },
        valid: false,
        touched: false
      },
      password: {
        elementType: 'input',
        elementConfig: {
          type: 'Password',
          placeholder: 'Password'
        },
        value: '',
        validation: {
          required: true,
          minLength: 6
        },
        valid: false,
        touched: false
      }
    },
    isLoggedIn: false,
    signInEnable: false,
    error: false,
    errorMsg: null
  }

  errorHandler = (errMsg) => {
    // console.log(errMsg.data.error)
    if (errMsg) {
      this.setState({
        error: true,
        errorMsg: errMsg.data.error
      })
    }
  }

  setLocalStorage = (res) => {
    
    localStorage.setItem('token', res.idToken)
    localStorage.setItem('userId', res.localId)
    console.log(res.idToken)
    console.log(res)
    this.props.hideLogin(res)
    this.props.getLoggedUserData();
  }

  submitHandler = (event) => {
    event.preventDefault();
    let url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyCMeJqviV77T6kwyOfLY47zt9jaO1Rriu4'

    if (this.state.signInEnable) {
      url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyCMeJqviV77T6kwyOfLY47zt9jaO1Rriu4'
    }
    const loginData = {
      email: this.state.users.email.value,
      password: this.state.users.password.value,
      returnSecureToken: true
    };
    axios.post(url, loginData)
      .then(res => this.setLocalStorage(res.data))
      .catch(error => {
        this.errorHandler(error.response)
      })
  }

  isValid = (val, userKey) => {
    let isValid = true;
    if (!userKey) {
      return true;
    }
    if (userKey.required) {
      isValid = val.trim() !== '' && isValid;
    }
    if (userKey.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = pattern.test(val) && isValid
    }

    if (userKey.minLength) {
      isValid = val.length >= userKey.minLength && isValid
    }

    return isValid;
  }

  inputChangedHandler = (event, userKey) => {
    const updatedUsers = {
      ...this.state.users,
      [userKey]: {
        ...this.state.users[userKey],
        value: event.target.value,
        valid: this.isValid(event.target.value, this.state.users[userKey].validation),
        touched: true
      }
    }
    this.setState({ users: updatedUsers })
  }

  signUpSignInHandler = () => {
    
    this.setState(prevState => {
      return { signInEnable: !prevState.signInEnable }
    })
  }
  render() {
    const inputsArray = [];
    for (let key in this.state.users) {
      inputsArray.push({
        id: key,
        config: this.state.users[key]
      });
    }
    let inputElement = inputsArray.map(input => (
      <Input
        key={input.id}
        elementType={input.config.elementType}
        elementConfig={input.config.elementConfig}
        value={input.config.value}
        invalid={!input.config.valid}
        shouldValidate={input.config.validation}
        touched={input.config.touched}
        changed={(event) => this.inputChangedHandler(event, input.id)} />
    ));


    let errorMessage = null;

    if (this.state.error) {
      errorMessage = this.state.errorMsg.message.split('_').join(' ');
      errorMessage = (
        <p className="center errorMsg">{errorMessage}</p>
      )
    }

    const signIn = this.state.signInEnable;
    const submit = this.submitHandler;
    return (
      <Fragment>
        <Backdrop clicked={this.props.loginHandler} show={this.props.loginShow} />
        <div className={this.props.loginShow ? 'Login showMe' : 'Login'}>
          <i className="material-icons closeLogin" onClick={this.props.loginHandler}>clear</i>
          {errorMessage}
          <form onSubmit={submit}>
            {inputElement}
            <Button buttonClass='btn teal darken-2 right'>{signIn ? 'Sign In' : 'Sign Up'}</Button>
            <div className="changeLog" onClick={this.signUpSignInHandler}>
              {!signIn ? 'I already have account' : 'Back to sign up'}
            </div>
          </form>
        </div>
      </Fragment>
    )
  }
}

export default Login;